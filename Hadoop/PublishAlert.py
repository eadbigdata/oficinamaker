#!/usr/bin/python3

import paho.mqtt.client as mqtt
import sys
import time

# MQTT Connection Info
username = 'edtkbude'
password = 'HV1M4WsYfrPn'
address  = 'm16.cloudmqtt.com'
port     = 10927

client = mqtt.Client('hadoopconsumer')
client.username_pw_set(username, password)
client.connect(address,port)

alerts_queue   = 'alerta'

hash           = '7F5205100199477C2B2C88E9E91E98C4'

state = 'off'

if sys.argv[1] == 'on' or sys.argv[1] == 1:
	state = 'on'

print("Setting state to: " + state)
client.publish(alerts_queue, "{};{}".format(hash, state))
