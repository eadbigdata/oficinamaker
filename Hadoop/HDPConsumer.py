#!/usr/bin/python3 -u

import paho.mqtt.client as mqtt
import os
import sys
import time

# MQTT Connection Info
username = 'edtkbude'
password = 'HV1M4WsYfrPn'
address  = 'm16.cloudmqtt.com'
port     = 10927

os.environ['HADOOP_USER_NAME'] = 'hdfs' 

dir = 'hdfs://sandbox-hdp/OficinaMaker'

client = mqtt.Client('hadoopalerter')
client.username_pw_set(username, password)
client.connect(address,port)
client.loop_start()

instant_queue  = 'valores_instantaneos'
averages_queue = 'valores_medios'
alerts_queue   = 'alerta'

hash           = '7F5205100199477C2B2C88E9E91E98C4'

# Quando precisar enviar uma mensagem para um módulo, função está pronta
def send_alert(hash, state):
    if state == 'on' or state == 1:
        state = 'on'
    else:
        state = 'off'

    print("Changing state of {} to {}".format(hash, state))
    client.publish(alerts_queue, "{};{}".format(hash, state))

def on_message(mosq, obj, msg):
    print("Received Message", msg.payload, msg.topic)
    # Append to HDFS File with same name as queue .csv
    file = dir + '/' + msg.topic + '.csv'
    message = str(msg.payload.decode("utf-8"))
    print("Appending {} to {}".format(message, file))
    os.system("echo '" + message + "' | hadoop fs -appendToFile - " + file)

client.subscribe(instant_queue)
client.subscribe(averages_queue)
client.on_message = on_message

rc = 0

while rc == 0:
    rc = client.loop()
    time.sleep(1)
