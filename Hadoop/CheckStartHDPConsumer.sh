#!/bin/bash

pgrep HDPConsumer.py || (
	echo "HDPConsumer is not running - starting it"
	/root/OficinaMaker/Hadoop/HDPConsumer.py >> /root/logs/HDPConsumer.log 2>&1 &
)
