#!/usr/bin/env python

import sys
import operator

localizacao = {}
problemas = 0
medias = {}

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ latitude, longitude, count, tempsum ] = linha.split(";")

        position = latitude + ';' + longitude;

        if position in localizacao:
            localizacao[position]["count"]   += int(count)
            localizacao[position]["tempsum"] += int(tempsum)
        else:
            localizacao[position] = { "count" : int(count), "tempsum" : float(tempsum) }
    except Exception as e: 
        problemas += 1
        print "Exception: {}".format(e)


for position in localizacao:
    medias[position] = localizacao[position]["tempsum"] / localizacao[position]["count"]

sorted_medias = sorted(medias.items(), key=operator.itemgetter(1),reverse=True)

i = 0
while i < len(sorted_medias) and i < 10:
    [ latitude, longitude ] = sorted_medias[i][0].split(";")
    media = sorted_medias[i][1]
    print "Latitude: {}, Longitude: {}, Media de Temperatura: {}".format(latitude, longitude, media)
    i += 1
