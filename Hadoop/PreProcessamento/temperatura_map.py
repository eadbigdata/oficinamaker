#!/usr/bin/env python

import sys

problemas = 0

anos = {}
for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ id, hora, minuto, ano, mes, dia, temperatura, latitude, longitude ] = linha.split(";")
        if ano in anos:
            anos[ano]["count"]   += 1
            anos[ano]["tempsum"] += float(temperatura)
        else:
            anos[ano] = { "count" : 1, "tempsum" : float(temperatura) }
    except Exception as e:
        problemas += 1
        print "Problema: {}".format(e)

for ano in anos:
    print "{};{};{}".format(ano, anos[ano]["count"], anos[ano]["tempsum"])
