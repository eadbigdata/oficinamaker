#!/usr/bin/env python

import sys

localizacao = {}
problemas = 0

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ id, hora, minuto, ano, mes, dia, temperatura, latitude, longitude ] = linha.split(";")
        latitude  = 0.05 * int (float(latitude)  / 0.05)
        longitude = 0.05 * int (float(longitude) / 0.05) 
        if not latitude in localizacao:
            localizacao[latitude] = {}

        if longitude in localizacao[latitude]:
            localizacao[latitude][longitude] += 1
        else:
            localizacao[latitude][longitude] = 1
    except Exception as e: 
        problemas += 1
        print "Exception: {}".format(e)

for latitude in localizacao:
    for longitude in localizacao[latitude]:
        print "{};{};{}".format(latitude, longitude, localizacao[latitude][longitude])

