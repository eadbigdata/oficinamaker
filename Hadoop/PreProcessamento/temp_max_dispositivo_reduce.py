#!/usr/bin/env python

import sys
import operator

dispositivos = {}
temperaturas = {}
problemas = 0

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ id, count, maxtemp, latitudesum, longitudesum ] = linha.split(";")
        maxtemp = float(maxtemp)

        if id in dispositivos:
            dispositivos[id]["count"] += count
            dispositivos[id]["latitudesum"]  += float(latitudesum)
            dispositivos[id]["longitudesum"] += float(longitudesum)
            if maxtemp > temperaturas[id]:
                temperaturas[id] = maxtemp
        else:
            dispositivos[id] = { "count" : int(count), "latitudesum" : float(latitudesum), "longitudesum" : float(longitudesum) }
            temperaturas[id] = maxtemp
    except Exception as e: 
        problemas += 1
        print "Exception: {}".format(e)

sorted_temperaturas = sorted(temperaturas.items(), key=operator.itemgetter(1), reverse=True)

i = 0
while i < len(sorted_temperaturas) and i < 10:
    id      = sorted_temperaturas[i][0]
    maxtemp = sorted_temperaturas[i][1]
    latitude_avg  = dispositivos[id]["latitudesum"]  / dispositivos[id]["count"]
    longitude_avg = dispositivos[id]["longitudesum"] / dispositivos[id]["count"]

    print "Dispositivo: {}, temperatura maxima: {}, latitude media: {}, longitude media: {}".format(id, maxtemp, latitude_avg, longitude_avg)

    i += 1
