#!/usr/bin/env python

import sys

dispositivos = {}
problemas = 0

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ id, hora, minuto, ano, mes, dia, temperatura, latitude, longitude ] = linha.split(";")
        dispositivos[id] = 1
    except:
        problemas += 1

for dispositivo in dispositivos:
    print dispositivo
