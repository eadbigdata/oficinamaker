#!/usr/bin/env python

import sys
import re

dispositivos = {}
problemas = 0

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ id, hora, minuto, ano, mes, dia, temperatura, latitude, longitude ] = linha.split(";")

        if re.match('\d', id):
            continue

        hora        = int(hora)
        minuto      = int(minuto)
        temperatura = float(temperatura)
        latitude    = float(latitude)
        longitude   = float(longitude)

        if id in dispositivos:
            dispositivos[id]["count"]     += 1
            dispositivos[id]["hora"]      += hora
            dispositivos[id]["minuto"]    += minuto
            dispositivos[id]["tempsum"]   += temperatura
            dispositivos[id]["latitude"]  += latitude
            dispositivos[id]["longitude"] += longitude

            if temperatura < dispositivos[id]["tempmin"]:
                dispositivos[id]["tempmin"] = temperatura

            if temperatura > dispositivos[id]["tempmax"]:
                dispositivos[id]["tempmax"] = temperatura
        else:
            dispositivos[id] = { "count" : 1, "hora" : hora, "minuto" : minuto, "tempmin" : temperatura, "tempmax" : temperatura, 
                                 "tempsum" : temperatura, "latitude" : latitude, "longitude" : longitude }
                
    except Exception as e: 
        problemas += 1
        print "Exception: {}".format(e)

for id in dispositivos:
    d = dispositivos[id]
    print "{};{};{};{};{};{};{};{};{}".format(id, d["count"], d["hora"], d["minuto"], d["tempmin"], d["tempmax"], d["tempsum"], d["latitude"], d["longitude"])
