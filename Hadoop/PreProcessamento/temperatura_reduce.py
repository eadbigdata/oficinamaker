#!/usr/bin/env python

import sys

problemas = 0

anos = {}
for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ ano, count, tempsum ] = linha.split(";")
        if ano in anos:
            anos[ano]["count"]   += int(count)
            anos[ano]["tempsum"] += float(tempsum)
        else:
            anos[ano] = { "count" : int(count), "tempsum" : float(tempsum) }
    except Exception as e:
        problemas += 1
        print "Problema: {}".format(e)

for ano in anos:
    print "Ano: {} Temperatura Media: {}".format(ano, anos[ano]["tempsum"] / anos[ano]["count"])
