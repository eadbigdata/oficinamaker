#!/usr/bin/env python

import sys
import operator

localizacao = {}
problemas = 0

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ latitude, longitude, count ] = linha.split(";")

        position = latitude + ';' + longitude;

        if position in localizacao:
            localizacao[position] += int(count)
        else:
            localizacao[position] = int(count)
    except Exception as e: 
        problemas += 1
        print "Exception: {}".format(e)


sorted_localizacao = sorted(localizacao.items(), key=operator.itemgetter(1),reverse=True)

i = 0
while i < len(sorted_localizacao) and i < 10:
    [ latitude, longitude ] = sorted_localizacao[i][0].split(";")
    count = sorted_localizacao[i][1]
    print "Latitude: {}, Longitude: {}, Count: {}".format(latitude, longitude, count)
    i += 1
