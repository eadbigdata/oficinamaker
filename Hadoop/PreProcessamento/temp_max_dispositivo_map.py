#!/usr/bin/env python

import sys

dispositivos = {}
problemas = 0

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ id, hora, minuto, ano, mes, dia, temperatura, latitude, longitude ] = linha.split(";")

        temperatura = float(temperatura)

        if id in dispositivos:
            dispositivos[id]["count"] += 1
            dispositivos[id]["latitudesum"]  += float(latitude)
            dispositivos[id]["longitudesum"] += float(longitude)
            if temperatura > dispositivos[id]["maxtemp"]:
                dispositivos[id]["maxtemp"] = temperatura
        else:
            dispositivos[id] = { "count" : 1, "maxtemp" : float(temperatura), "latitudesum" : float(latitude), "longitudesum" : float(longitude) }

    except Exception as e: 
        problemas += 1
        print "Exception: {}".format(e)

for id in dispositivos:
    print "{};{};{};{};{}".format(id, dispositivos[id]["count"], dispositivos[id]["maxtemp"], dispositivos[id]["latitudesum"], dispositivos[id]["longitudesum"])

