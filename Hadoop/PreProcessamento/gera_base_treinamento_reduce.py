#!/usr/bin/env python

import sys
import re

dispositivos = {}
problemas = 0

for linha in sys.stdin:
    linha = linha.strip()

    try:
        [ id, count, hora, minuto, tempmin, tempmax, tempsum, latitude, longitude ] = linha.split(";")

        count     = int(count)
        hora      = int(hora)
        minuto    = int(minuto)
        tempmin   = float(tempmin)
        tempmax   = float(tempmax)
        tempsum   = float(tempsum)
        latitude  = float(latitude)
        longitude = float(longitude)

        if id in dispositivos:
            dispositivos[id]["count"]     += count
            dispositivos[id]["hora"]      += hora
            dispositivos[id]["minuto"]    += minuto
            dispositivos[id]["tempsum"]   += tempsum
            dispositivos[id]["latitude"]  += latitude
            dispositivos[id]["longitude"] += longitude

            if temperatura < dispositivos[id]["tempmin"]:
                dispositivos[id]["tempmin"] = tempmin

            if temperatura > dispositivos[id]["tempmax"]:
                dispositivos[id]["tempmax"] = tempmax
        else:
            dispositivos[id] = { "count" : count, "hora" : hora, "minuto" : minuto, "tempmin" : tempmin, "tempmax" : tempmax, 
                                 "tempsum" : tempsum, "latitude" : latitude, "longitude" : longitude }
                
    except Exception as e: 
        problemas += 1
        print "Exception: {}".format(e)

for id in dispositivos:
    d = dispositivos[id]
    c = d["count"]
    hora      = d["hora"]      / c
    minuto    = d["minuto"]    / c
    latitude  = d["latitude"]  / c
    longitude = d["longitude"] / c
    tempavg   = d["tempsum"]   / c

    if tempavg < 10:
        classe = "Frio"
    elif tempavg < 20:
        classe = "Moderado"
    elif tempavg < 25:
        classe = "Quente"
    else:
        classe = "Alerta"

    print "{};{};{};{};{};{};{};{}".format(id, hora, minuto, d["tempmin"], d["tempmax"], latitude, longitude, classe)
