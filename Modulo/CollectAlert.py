#!/usr/bin/python3

import statistics as stat
import random
import time
from datetime import datetime
import RPi.GPIO as GPIO
import Adafruit_DHT as dht
import paho.mqtt.client as mqtt

pinoRele = 32

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(pinoRele,GPIO.OUT)

hash           = '7F5205100199477C2B2C88E9E91E98C4'
alerts_queue   = 'alerta'

def on_message(mosq, obj, msg):
    print(msg.payload, msg.topic)
    if msg.topic == alerts_queue:
        message = str(msg.payload.decode("utf-8"))
        dst_hash, new_state = message.split(';')
        print("Got Message for " + dst_hash + " new state is " + new_state)
        if dst_hash == hash:
            print("Message is for me - Urray")
            if new_state == 'on':
                print("Alert mode on - turning on LED")
                GPIO.output(pinoRele, GPIO.HIGH) 
            else:
                print("Alert mode off - turning off LED")
                GPIO.output(pinoRele, GPIO.LOW)
                

# MQTT Connection Info
username = 'edtkbude'
password = 'HV1M4WsYfrPn'
address  = 'm16.cloudmqtt.com'
port     = 10927

client = mqtt.Client()
client.username_pw_set(username, password)
client.connect(address,port)
client.subscribe(alerts_queue)
client.on_message = on_message
client.loop_start()

print("Connected to MQTT")

while True:
    time.sleep(1)
