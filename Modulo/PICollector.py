#!/usr/bin/python3 -u

import statistics as stat
import random
import time
from datetime import datetime
import RPi.GPIO as GPIO
import Adafruit_DHT as dht
import paho.mqtt.client as mqtt

pinoRele = 32

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(pinoRele,GPIO.OUT)

# MQTT Connection Info
username = 'edtkbude'
password = 'HV1M4WsYfrPn'
address  = 'm16.cloudmqtt.com'
port     = 10927

client = mqtt.Client()
client.username_pw_set(username, password)
client.connect(address,port)
client.loop_start()

instant_queue  = 'valores_instantaneos'
averages_queue = 'valores_medios'
alerts_queue   = 'alerta'

client.subscribe(alerts_queue)

hash           = '7F5205100199477C2B2C88E9E91E98C4'
latitude  = latitude_base  = -25.406155
longitude = longitude_base = -49.253824

start = (datetime.now())

last_random_change = 0 # Mudamos a cada inicialização
last_avg_publish   = int("{:04d}{:02d}{:02d}{:02d}".format(start.year, start.month, start.day, start.hour))

hours   = []
minutes = []
temps   = []
lats    = []
longs   = []

def on_message(mosq, obj, msg):
    print("Received Message", msg.payload, msg.topic)
    if msg.topic == alerts_queue:
        print("Alert Queue - Checking if I'm the destination")
        message = str(msg.payload.decode("utf-8"))
        dst_hash, new_state = message.split(';')
        if dst_hash == hash:
            print("Message is for me - changing indicator to: " + new_state)
            if new_state == 'on':
                GPIO.output(pinoRele, GPIO.HIGH) 
            else:
                GPIO.output(pinoRele, GPIO.LOW)
                

client.subscribe(alerts_queue)
client.on_message = on_message

print(start)
print("Execution started")

while True:
    # Fields: HASH,HORA,MINUTO,ANO,MES,DIA,TEMP,LAT,LONG
    now = (datetime.now())

    # Devemos mudar a posicao a cada dia - ou seja, mudaremos quando tivermos um novo ANOMESDIA
    dia = int("{:04d}{:02d}{:02d}".format(now.year, now.month, now.day))
    if dia > last_random_change:
        last_random_change = dia
        latitude  = latitude_base  + random.uniform(-0.05,0.05)
        longitude = longitude_base + random.uniform(-0.05,0.05)

    hour = int("{:04d}{:02d}{:02d}{:02d}".format(now.year, now.month, now.day, now.hour))
    # Estamos em uma hora nova com pelo menos 10 coletas - enviamos as medias, minimos e maximos e resetamos
    if hour > last_avg_publish and len(hours) > 10:
        last_avg_publish = hour
        averages_publish_str = "{};{};{};{};{};{};{}".format(hash, stat.mean(hours), stat.mean(minutes), min(temps), max(temps), stat.mean(lats), stat.mean(longs))
        client.publish(averages_queue, averages_publish_str)
        print(averages_queue + ";" + averages_publish_str)
        hours   = []
        minutes = []
        temps   = []
        lats    = []
        longs   = []
        
    umid, temp = dht.read_retry(dht.DHT11, 4)

    instant_publish_str = "{};{};{};{};{};{};{};{};{}".format(hash, now.hour, now.minute, now.year, now.month, now.day, temp, latitude, longitude)
    client.publish(instant_queue, instant_publish_str)
    print(instant_queue + ";" + instant_publish_str)

    hours.append(now.hour)
    minutes.append(now.minute)
    temps.append(temp)
    lats.append(latitude)
    longs.append(longitude)

    time.sleep(60 - now.second) # Até o proximo minuto cheio
