#!/bin/bash

pgrep PICollector.py || (
	echo "PICollector is not running - starting it"
	/home/pi/OficinaMaker/Modulo/PICollector.py >> /home/pi/logs/PICollector.log 2>&1 &
)
